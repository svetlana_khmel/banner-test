# This project was created to test webpack-react-test-banner npm module.

webpack-react-test-banner it a React component available throw npm.
Component can be used both for the React and non React project.

It can be configured throw admin:

* https://back-for-test-banner-react-npm.herokuapp.com

 (see this repo https://gitlab.kfplc.com/KITS-UI-Development/test-banner-component-for-react-back )
 
 
  Banner component is here:
 
 * https://gitlab.kfplc.com/KITS-UI-Development/react-test-banner-library



 ** Run project **

 ```

 npm install

 npm copy

 npm start


 ```

 
 ** Tools used on project: **
 
* Express as server
* Grunt as bulder
* browserify for commonjs


**  !!!!! Below I put down notes related to react-test-banner-library ***

(sources: https://gitlab.kfplc.com/KITS-UI-Development/react-test-banner-library)

* react-test-banner-library in a npm module. This component can be usen with React projects as React compotent and as library on non-React projects.


** How to use react-test-banner-library on React projects **

1) Install component

```
npm install webpack-react-test-banner

```

2) Use it in you React project



```
import TestBannerComponent from 'webpack-react-test-banner';

```


```
render () {
        return (
                <TestBannerComponent />
        )
    }

```

** How to use react-test-banner-library on non-React projects **

This particular project is created to demonstrate usage webpack-react-test-banner on non react projects.

1) Attach 'webpack-react-test-banner' library (wrapper.js).

2) Invoke component after DOM is read (see example: public/js/script.js).

"content" is an id in public/index.html


```
<div id="content"></div>

```

```
     window.webpackReactTestBanner("content")();

```

(This particular project is building with browserify, so Grunt takes sources from /public/js and put it as bundle to /build/bundle.js)


**  !!!!! Below I explain how react-test-banner-library builds ***

VERY IMPORTANT for package creating:


libraryTarget: 'window', // THIS IS THE MOST IMPORTANT LINE! here you set where you find you component
library: 'webpackReactTestBanner',
libraryExport: 'default // it is important too


*** To build react-test-banner-library run ***

```
npm run module

```

***  To publish to NPM ***

Change package version in package.json. You should be loggined in on npm.

```

npm publish

```




*** Articles to read:

https://webpack.js.org/guides/author-libraries/

https://medium.com/@sevenleaps/using-react-components-in-non-react-websites-12fb7d71b63f

https://github.com/sevenleaps/react-webpack-wrapper-example

https://codeburst.io/building-react-widget-libraries-using-webpack-e0a140c16ce4

https://webpack.js.org/configuration/output/



* To publish on npm

!!!!!Very important:
We use libraryTarget: 'commonjs2', for publishing REACT component, 

so run npm run dev

We use libraryTarget: 'window', for publishing NON  REACT component,



1) npm run dev 

2) change version in package.json

3) npm publish

(You should be loginned on npm)





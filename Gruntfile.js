'use strict';

module.exports = function(grunt) {
  //Load all grunt tasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-babel');

  //require('load-grunt-tasks')(grunt);  //Now we can load all tasks this way (npm module load-grunt-tasks)

  grunt.initConfig({
    watch: {
      js: {
        files: ['public/js/*.js'],
        tasks: ['browserify' ]
      }
    },
    browserify: {
      js: {
        src: 'public/js/*.js',
        dest: 'public/build/bundle.js'
      }
    },
    babel: {
      options: {
        plugins: ['transform-react-jsx'],
        presets: ['es2015', 'react']
      },
      jsx: {
        files: [{
          expand: true,
          cwd: 'public/admin/js', // Custom folder
          src: ['*.jsx'],
          dest: 'public/js/', // Custom folder
          ext: '.js'
        }]
      }
    }
  });

  //the default task running 'grunt' in console is 'watch'
  grunt.registerTask('default', ['watch', 'babel', 'browserify']);
  grunt.registerTask('copy', ['browserify']);
};
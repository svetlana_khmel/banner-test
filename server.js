var express = require('express');
var path = require('path');

var app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (res, req) {
    req.render('index.html');
})


const port = process.env.PORT || 3001;

app.listen(port, function () {
  console.log('Example app listening on port ',port);
});

